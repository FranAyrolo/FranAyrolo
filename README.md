# Personal README
##### Work in progress

#### Welcome to my heap of repositories. Here are some projects that are more showable than the rest:
#### **(Click/tap the images to follow the links)**

## Void in One!
[<img width="350" src="https://gitlab.com/FranAyrolo/FranAyrolo/-/raw/main/void_in_one_screenshot.png">](https://estroponcio.itch.io/void-in-one)

I made a little game for the [69th Godot Wild Jam](https://itch.io/jam/godot-wild-jam-69 "69th Godot Wild Jam")
It's a sort of physics-based puzzle game about golfing in space. Right now it looks quite crude, but I'm working on adding a proper settings menu, a tutorial level, more levels, and some QoL features like trajectory calculation.
Stay Tuned!

You can also follow me on itch, here: 
[<img width="100" src="https://gitlab.com/FranAyrolo/FranAyrolo/-/raw/main/itch_profile.png">](https://estroponcio.itch.io/)

---
## Rewriting stuff in Rust: Raytracing edition
[<img width="350" src="https://gitlab.com/FranAyrolo/FranAyrolo/-/raw/main/raytracing_rust.jpeg">](https://gitlab.com/FranAyrolo/OK_now-rewrite-it-in-rust/-/tree/main/raytracing_1_weekend)

In order to learn the Rust language, I'm reinventing the wheel that I *also* want to learn, a simple raytracing learning project from [the Raytracing in One Weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html) series.
I'm currently moving through the first project, the "in one weekend", but I'm planning to move onto the next 2 parts with Rust.
